import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Appointment } from './Appointment';

@Injectable({
  providedIn: 'root',
})
export class AppointmentsService {
  private BASE_URL = environment.API_URL;

  constructor(private http: HttpClient) {}

  getAppointments(): Observable<Appointment[]> {
    return this.http.get<Appointment[]>(`${this.BASE_URL}/appointments`);
  }

  findAppoinment(cc: string): Observable<any> {
    return this.http.post<Appointment[]>(
      `${this.BASE_URL}/find-users-appointments`,
      {
        cc,
      }
    );
  }

  createAppointment(
    appointmentDate: string,
    cc: string,
    name: string,
    email: string,
    hour: string,
    phone: string,
    service: string
    
  ): Observable<Appointment> {
    return this.http.post<Appointment>(`${this.BASE_URL}/appointments`, {
      appointmentDate,
      cc,
      name,
      email,
      hour,
      phone,
      service,
    });
  }

  sendMail(
    appointmentDate: string,
    cc: string,
    name: string,
    email: string,
    hour: string,
    phone: string
  ): Observable<any> {
    return this.http.post(`${this.BASE_URL}/mailer`, {
      appointmentDate,
      cc,
      name,
      email,
      hour,
      phone,
    });
  }

  cancelAppointment(id: string): Observable<any> {
    return this.http.delete(`${this.BASE_URL}/appointments/${id}`);
  }

  filterAppointment(appointmentDate: string): Observable<any> {
    return this.http.post<Appointment[]>(
      `${this.BASE_URL}/filter-appointments`,
      {
        appointmentDate,
      }
    );
  }

  login(user: string, pass: string): Observable<any> {
    return this.http.post(`${this.BASE_URL}/login`, {
      user,
      pass,
    });
  }
}
