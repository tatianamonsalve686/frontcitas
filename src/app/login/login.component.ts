import { Component, OnInit } from '@angular/core';
import { AppointmentsService } from '../appointments.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public successMsg!: string;
  public errorMsg!: string;
  user!: string;
  pass!: string;
  constructor(
    private appointmentsService: AppointmentsService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  login() {
    this.successMsg = '';
    this.errorMsg = '';
    this.appointmentsService.login(this.user, this.pass).subscribe(
      () => {
        this.router.navigate(['/appointment-list']);
      },
      (error: ErrorEvent) => {
        this.errorMsg = "Autenticacion errada";
      }
    );
  }
}
