import { Component, OnInit } from '@angular/core';
import { AppointmentsService } from '../appointments.service';
import { Appointment } from '../Appointment';

interface Hour {
  value: string;
  viewValue: string;
  disabled?: boolean;
}

interface Service {
  value: string;
  viewValue: string;
  disabled?: boolean;
}
@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css'],
})
export class AppointmentComponent implements OnInit {
  services: Service[] = [
    { value: 'cambio_aceite', viewValue: 'Cambio de aceite' },
    { value: 'revision', viewValue: 'Revisión' },
    { value: 'cambio_frenos', viewValue: 'Cambio de frenos' },
    { value: 'mantenimiento_preventivo', viewValue: 'Mantenimiento preventivo' },
  ];
  hours: Hour[] = [
    { value: '8', viewValue: '08:00 am' },
    { value: '9', viewValue: '09:00 am' },
    { value: '10', viewValue: '10:00 am' },
    { value: '11', viewValue: '11:00 am' },
    { value: '12', viewValue: '12:00 pm' },
    { value: '14', viewValue: '02:00 pm' },
    { value: '15', viewValue: '03:00 pm' },
    { value: '16', viewValue: '04:00 pm' },
    { value: '17', viewValue: '05:00 pm' },
    { value: '18', viewValue: '06:00 pm' },
  ];

  public successMsg!: string;
  public errorMsg!: string;
  appointmentDate!: string;
  cc!: string;
  name!: string;
  email!: string;
  hour!: string;
  phone!: string;
  service!: string;
  public appointments!: Appointment[];
  public loading = true;

  constructor(private appointmentService: AppointmentsService) {}

  ngOnInit() {}

  disableSelectedHours() {
    this.appointments.forEach((appoiment) => {
      let hourFound = this.hours.find((hour) => appoiment.hour == hour.value);
      if (hourFound != undefined) {
        hourFound.disabled = true;
      }
    });
  }
  createAppointment() {
    this.successMsg = '';
    this.errorMsg = '';
    this.appointmentService
      .createAppointment(
        this.appointmentDate,
        this.cc,
        this.name,
        this.email,
        this.hour,
        this.phone,
        this.service
      )
      .subscribe(
        (createdAppointment: Appointment) => {
          this.sendMail(createdAppointment), (this.cc = '');
          this.appointmentDate = '';
          this.name = '';
          this.email = '';
          this.hour = '';
          this.phone = '';
          this.service = '';

          const appointmentDate = new Date(
            createdAppointment.appointmentDate
          ).toLocaleDateString();
          const appointmentName = createdAppointment.name;
          const appointmentEmail = createdAppointment.email;
          const appointmentHour = createdAppointment.hour;
          const appointmentPhone = createdAppointment.phone;

          this.successMsg = `Cita agendada para el dia: ${appointmentDate} a las ${appointmentHour} horas , por favor revise su correo ${appointmentEmail}`;
          setTimeout(() => {
            this.successMsg = ``;
          }, 5000);
        },
        (error: ErrorEvent) => {
          this.errorMsg = error.error.message;
        }
      );
  }

  sendMail(createdAppointment: Appointment) {
    this.appointmentService
      .sendMail(
        new Date(createdAppointment.appointmentDate).toLocaleDateString(),
        createdAppointment.cc,
        createdAppointment.name,
        createdAppointment.email,
        createdAppointment.hour,
        createdAppointment.phone
      )
      .subscribe(
        () => {},
        (error: ErrorEvent) => {
          this.errorMsg = error.error.message;
        }
      );
  }

  filterAppointment() {
    this.appointmentService.filterAppointment(this.appointmentDate).subscribe(
      (appointments: Appointment[]) => {
        this.appointments = appointments;
        this.loading = false;
        if (appointments.length > 0) {
          this.disableSelectedHours();
        } else {
          this.hours = [
            { value: '8', viewValue: '08:00 am' },
            { value: '9', viewValue: '09:00 am' },
            { value: '10', viewValue: '10:00 am' },
            { value: '11', viewValue: '11:00 am' },
            { value: '12', viewValue: '12:00 pm' },
            { value: '14', viewValue: '02:00 pm' },
            { value: '15', viewValue: '03:00 pm' },
            { value: '16', viewValue: '04:00 pm' },
            { value: '17', viewValue: '05:00 pm' },
            { value: '18', viewValue: '06:00 pm' },
          ];
        }
      },
      (error: ErrorEvent) => {
        this.errorMsg = error.error.message;
        this.loading = false;
      }
    );
  }
}
