import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentListComponent } from './appointment-list/appointment-list.component';
import { HomeComponent } from './home/home.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { ConsultAppointmentMainComponent } from './consult-appointment-main/consult-appointment-main.component';
import { FilteredAppointmentListComponent } from './filtered-appointment-list/filtered-appointment-list.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'appointment-list',
    component: AppointmentListComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'app-appointment',
    component: AppointmentComponent,
  },
  {
    path: 'consult-appointment',
    component: ConsultAppointmentMainComponent,
  },
  {
    path: 'app-filtered-appointment-list',
    component: FilteredAppointmentListComponent,
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
