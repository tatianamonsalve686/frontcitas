export interface Appointment{
  _id : string;
  cc : string;
  appointmentDate : string;
  name : string;
  email : string;
  hour : string;
  phone : string;
}
