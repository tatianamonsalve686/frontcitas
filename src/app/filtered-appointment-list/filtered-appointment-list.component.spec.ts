import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredAppointmentListComponent } from './filtered-appointment-list.component';

describe('FilteredAppointmentListComponent', () => {
  let component: FilteredAppointmentListComponent;
  let fixture: ComponentFixture<FilteredAppointmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilteredAppointmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredAppointmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
