import { Component, OnInit } from '@angular/core';
import { Appointment } from '../Appointment';
import { mergeMap } from 'rxjs/operators';
import { AppointmentsService } from '../appointments.service';

@Component({
  selector: 'app-filtered-appointment-list',
  templateUrl: './filtered-appointment-list.component.html',
  styleUrls: ['./filtered-appointment-list.component.css']
})
export class FilteredAppointmentListComponent implements OnInit {

  public loading = true;
  public cc!: string;
  public appointmentDate!: string;
  public errorMsg!: string;
  public successMsg!: string;
  public filteredAppointments!: Appointment[];
  public columns = ['appointmentDate', 'name', 'phone', 'email', 'hora', 'cancel'];
  public appointments!: Appointment[];

  constructor(private appointmentService: AppointmentsService) { }

  ngOnInit(): void {
    this.findByCc("vacio")
  }

  findAppoinment() {
    this.findByCc(this.cc)
  }

  findByCc(cc: string){
    this.appointmentService.findAppoinment(cc)
    .subscribe((appointments: Appointment[]) => {
      this.filteredAppointments = appointments;
      this.loading = false;
    },
    (error: ErrorEvent) => {
      this.errorMsg = error.error.message;
      this.loading = false;
    });
  }
  cancelAppointment(id: string) {
    this.appointmentService.cancelAppointment(id)
      .pipe(
        mergeMap(() => this.appointmentService.getAppointments())
      )
      .subscribe((appointments: Appointment[]) => {
        this.appointments = appointments;
        this.successMsg = 'Cita cancelada correctamente';
        this.findByCc("vacio")
      },
      (error: ErrorEvent) => {
        this.errorMsg = error.error.message;
      });
  }

}
