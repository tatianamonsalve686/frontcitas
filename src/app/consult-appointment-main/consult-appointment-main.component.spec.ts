import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultAppointmentMainComponent } from './consult-appointment-main.component';

describe('ConsultAppointmentMainComponent', () => {
  let component: ConsultAppointmentMainComponent;
  let fixture: ComponentFixture<ConsultAppointmentMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultAppointmentMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultAppointmentMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
