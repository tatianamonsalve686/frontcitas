import { Component, OnInit } from '@angular/core';
import { AppointmentsService } from '../appointments.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consult-appointment-main',
  templateUrl: './consult-appointment-main.component.html',
  styleUrls: ['./consult-appointment-main.component.css']
})
export class ConsultAppointmentMainComponent implements OnInit {

  public successMsg!: string;
  public errorMsg!: string;
  mail!: string;

  constructor(
    private appointmentsService: AppointmentsService,
    private router: Router
  ) {}

  ngOnInit(): void {
  }

  findAppoinment() {
    this.router.navigate(['/app-filtered-appointment-list']);
  }
}
